package com.paysly.modules.dwolla;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

/**
 * @author Hasan Diwan
 */
//@RunWith(SpringJUnit4ClassRunner.class)
public class AppTest {
	@Rule public ExpectedException thrown= ExpectedException.none();

	@Test
	public void testGoWithoutOAuthToken() {
		thrown.expect(NullPointerException.class);
		thrown.expectMessage("auth token missing, no default"); 
		new App().getBalance(null);
	}

	@Test
	public void testGoWithValidOAuthToken() {
		System.setProperty("dwolla.oauth", "NoD8ybmZv49NQpdxdDpi3yWrTcgZFruvpoUtgEKLtskoM3eArM");
		String amount = new App().getBalance(null);
		Assert.assertThat(amount, CoreMatchers.containsString(".00"));
	}
	@Test
	public void testValidSend() {
		boolean returnValue = new App().sendMoney("NoD8ybmZv49NQpdxdDpi3yWrTcgZFruvpoUtgEKLtskoM3eArM", "https://api-uat.dwolla.com/funding-sources/929e1066-3ad0-4799-9f0d-30121f71375d", "https://api-uat.dwolla.com/funding-sources/617c9d7f-84a8-4e1b-8bb5-831db7fde136", "4.00");
		Assert.assertTrue(returnValue);
	}

}

