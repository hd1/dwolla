package com.paysly.modules.dwolla;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import com.dwolla.java.sdk.responses.BalanceResponse;
import com.dwolla.java.sdk.DwollaServiceSync;
import com.dwolla.java.sdk.OAuthServiceSync;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import retrofit.RestAdapter; // FIXME remove dependency on this library

public class App {
	private static Logger LOG = Logger.getLogger("com.paysly.modules.dwolla.App");
	public App() {
   	}

	// FIXME use HTTP Client instead of RestAdapter
    public String getBalance(String oauth) {
		if (oauth == null) {
			oauth = System.getProperty("dwolla.oauth");
			if (oauth == null) {
				throw new NullPointerException("Oauth token missing, no default");
			}
		}

		double balance = new RestAdapter.Builder().setEndpoint("https://uat.dwolla.com/oauth/rest").build().create(DwollaServiceSync.class).getBalance(oauth).Response;
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		String accountBalance = nf.format(balance);
		Map<String,String> payload = new HashMap<>();
		payload.put("accountBalance", accountBalance);

		String json = null;
	    try {
			json = new ObjectMapper().writeValueAsString(payload);
		} catch (JsonProcessingException e) {
		}
		return json;
    }

	public Boolean sendMoney(String oauth, String sourceUrl, String destinationUrl, String amount) {
		Map<String,String> params = new HashMap<>();

		int status = -1;
		Map<String,String> links = new HashMap<>();
		links.put("source", sourceUrl);
		links.put("destination", destinationUrl);
		try {
			params.put("_links", new ObjectMapper().writeValueAsString(links));
		} catch (JsonProcessingException e) { }

		Map<String, String> amounts = new HashMap<>();
		amounts.put("currency", "USD"); 
		amounts.put("value", amount);
		try {
			params.put("amount", new ObjectMapper().writeValueAsString(amounts));
		} catch (JsonProcessingException e) { }

	 	HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead 

		try {
		 	HttpPost request = new HttpPost("https://api-uat.dwolla.com/transfers");
			request.setHeader(HttpHeaders.ACCEPT, "application/vnd.dwolla.v1.hal+json");
			request.setHeader(HttpHeaders.CONTENT_TYPE, "application/vnd.dwolla.v1.hal+json");
			request.setHeader(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", oauth));
		    StringEntity httpParams =new StringEntity(new ObjectMapper().writeValueAsString(params));
		    request.addHeader("content-type", "application/x-www-form-urlencoded");
			request.setEntity(httpParams);
			HttpResponse response = httpClient.execute(request);
			status = response.getStatusLine().getStatusCode();
			LOG.debug("Status code is "+status);
		} catch (Throwable t) {
			LOG.error(t.getMessage(), t);
		}
		return status == 201;
	}
}
